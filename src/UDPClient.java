import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;


public class UDPClient {
	static final int serverPort = 6789;
	static final int bufferSize = 1024;
	public static void main(String[] args) throws Exception {
		
		byte[] in = new byte[bufferSize];
		byte[] out = new byte[bufferSize];
		String serverName = "localhost";
		InetAddress serverIPAddress = InetAddress.getByName(serverName);
		DatagramSocket clientSocket = new DatagramSocket();
		clientSocket.connect(serverIPAddress, serverPort);
		BufferedReader userInput = new BufferedReader(new InputStreamReader(System.in));
		while (!userInput.ready()) {
			String outString = userInput.readLine();
			out = outString.getBytes();
	        DatagramPacket outPacket = new DatagramPacket(out, out.length, serverIPAddress, serverPort);
	        clientSocket.send(outPacket);
	        DatagramPacket inPacket = new DatagramPacket(in, in.length);
	        clientSocket.receive(inPacket);
	        String inString = new String(inPacket.getData());
	        System.out.println("FROM SERVER: " + inString);
		}
		clientSocket.close();
	}
}
