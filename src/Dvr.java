import java.io.File;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Scanner;
import java.util.Timer;
import java.util.TimerTask;


public class Dvr {
	
	//static final int serverPort = 6789;
	static final int bufferSize = 1024;
	//static final String nodeID = "A";
	static Timer timer;
	static final int pulseTime = 5;
	static int messageCount = 0;
	static int convergeCount = 0;
	static int deadCount = 0;
	static ArrayList <String> maybeDead = new ArrayList<String>();
	static ArrayList <String> toDeleteNode = new ArrayList<String>();
	static ArrayList<Neighbour> nodeList;
	static HashMap<String, Integer> seenCosts = new HashMap<String, Integer>();
	static HashMap<String, String> seenContent = new HashMap<String, String>();
	static HashMap<String, Integer> seenCount = new HashMap<String, Integer>();
	public static void main(String[] args) throws Exception {
		
		String nodeID = args[0];
		int serverPort = Integer.parseInt(args[1]);
		//setup neighbour list
		ArrayList<Neighbour> neighbourList = new ArrayList<Neighbour>();
		//ArrayList<Neighbour> nodeList = new ArrayList<Neighbour>();
		nodeList = new ArrayList<Neighbour>();
		//config file setup
		File pagePath = new File("src/config_files");
		File[] pageFiles = pagePath.listFiles();
		for (File f: pageFiles) {
			if (f.isFile()) {
				String currNodeID = f.getAbsolutePath().replaceFirst(".*/config", "");
				currNodeID = currNodeID.replaceFirst("\\..*", "");
				if (currNodeID.equals(nodeID)) {
					Scanner configReader = new Scanner(new File(f.getAbsolutePath()));
					StringBuilder configBuilder = new StringBuilder();
					while (configReader.hasNext()) {
						configBuilder.append(configReader.nextLine());
						configBuilder.append("\n");
					}
					configReader.close();
					String[] configLines = configBuilder.toString().split("\\n");
					System.out.println("Initializing Node: " + currNodeID);
					int lineCount = 0;
					int neighbourCount = 0;
					for (String s: configLines) {
						if (lineCount == 0) {
							// figure out how many neighbours
							neighbourCount = Integer.parseInt(s);
						} else {
							//setup neighbours
							String[] configChars = s.split(" ");
							if (configChars.length >= 3 && configChars[0] != null && configChars[1] != null && configChars[2] != null) {
								String neighbourID = configChars[0];
								int neighbourCost = Integer.parseInt(configChars[1]);
								int neighbourPort = Integer.parseInt(configChars[2]);
								Neighbour newNeighbour = new Neighbour(neighbourID, neighbourCost, neighbourPort, nodeID);
								neighbourList.add(newNeighbour);
							} else {
								System.out.println("ERROR: Could not initialise neighbour for " + currNodeID + " at line " + lineCount);
							}
						}
						lineCount++;
						//System.out.println(s);
					}
					if (neighbourCount != neighbourList.size()) {
						System.out.println("ERROR: Could not initialise neighbours for " + currNodeID);
					}
					//System.out.println(currNodeID + " has " + neighbourCount + " neighbours");
					//System.out.println("NEIGHBOUR LIST SIZE OF: " + neighbourList.size());
					
					// send out data to neighbouring nodes
					nodeList.addAll(neighbourList);
					setTimer(pulseTime, neighbourList, nodeID);
					
				}
			}
		}
		
		/* change above port number this if required */
		
		// create server socket
		@SuppressWarnings("resource")
		DatagramSocket socket = new DatagramSocket(serverPort);
		byte[] in = new byte[bufferSize];
		//byte[] out = new byte[bufferSize];
		
		while (true) {
			//ArrayList<Neighbour> prevList = new ArrayList<Neighbour>();
			//prevList.addAll(nodeList);
			
			DatagramPacket inPacket = new DatagramPacket(in, in.length);
		    socket.receive(inPacket);
		    String inStr = new String(inPacket.getData(), 0, inPacket.getLength());
		    System.out.println("FROM CLIENT: " + inStr);
		    inStr = inStr.trim();
		    
		    
		    String[] words = inStr.split("/n");
		    
		    String seenID = words[0];
		    seenID = seenID.replaceFirst(".* ", "");
		    deadCount++;
		    if (seenContent.containsKey(seenID) == false) {
		    	seenCosts.put(seenID, 1);
		    	seenContent.put(seenID, inStr);
		    	seenCount.put(seenID, 1);
		    } else {
		    	if (inStr.equals(seenContent.get(seenID))) {
		    		// if strings are equal just increment count
		    		seenCosts.put(seenID, seenCosts.get(seenID) + 1);
		    		seenCount.put(seenID, seenCount.get(seenID) + 1);
		    	} else {
		    		// otherwise reset cost and string values
		    		
		    		// IF WEVE SEEN SOMETHING FROM THIS NEIGHBOUR BEFORE, BUT THE STRING HAS CHANGED
		    		// WE MUST FIGURE OUT IF A NODE IS DEAD (AS THE NEIGHBOUR IS NOT TELLING US ABOUT IT ANYMORE).
		    		// CHECK TO SEE WHATS MISSING FROM THE STRING (POTENTIALLY DEAD NODES)
		    		// 
		    		
		    		
		    		Integer nodeCounter = 0;
		    		// words contains new Strings content
		    		String[] prevWords = seenContent.get(seenID).split("/n"); 
		    		for (String prevNode: prevWords) {
		    			if (nodeCounter == 0) {
		    				nodeCounter++;
		    				continue;
		    			}
		    			String possibleDeadNode = prevNode.replaceFirst(" .*", "");
		    			System.out.println("POTENTIALLY DEAD NODE IS FOUND: " + possibleDeadNode);
		    			Boolean isDead = true;
		    			Integer prevCounter = 0;
		    			
		    			
		    			for (String node: words) {
		    				if (prevCounter == 0) {
		    					prevCounter++;
		    					continue;
		    				}
		    				String nonDead = node.replaceFirst(" .*", "");
		    				if (nonDead.equals(possibleDeadNode)) {
		    					isDead = false;
		    					break;
		    				}
		    				//System.out.println("CURRENT NODE IS : " + nonDead);
		    				prevCounter++;
		    			}
		    			// WHEN WE FIND OUT A NEIGHBOURS PREVIOUS MESSAGE CONTAINS A NODE THAT THE CURRENT
		    			// MESSAGE DOES NOT, WE KNOW SOMETHING HAS CHANGED (A NODE HAS DIED OR SOMETHING).
		    			
		    			// IN THE ABOVE CASE, isDead == true 
		    			// THUS, WE MUST ITERATE THROUGH OUR nodeList, AND REMOVE ANYTHING THAT IS TO
		    			// OR VIA THAT NODE
		    			// AFTER REMOVING THE APPROPRIATE ITEMS, WE MUST THEN REFILL ANY NECESSARY VALUES
		    			// FROM THE NEIGHBOURS LIST
		    			
		    			// ESSENTIALLY: ANY VALUE THAT IS MISSING FROM CURRENT BUT IN PREVIOUS
		    			// MUST BE MISSING DUE TO ITS "VIA" NODE BEING DEAD AT THIS CURRENT TIME (IN TERMS OF ITS CURRENT VIA)
		    			// THUS, WE MUST REMOVE THAT LINK COST AND POTENTIALLY REPLACE IT AFTER FROM OUR NEIGHBOURLIST COSTS
		    			if (isDead == true) {
		    				if (maybeDead.contains(possibleDeadNode) == false) {
		    					System.out.println("POTENTIAL DEAD NODE FOUND: " + possibleDeadNode);
		    					maybeDead.add(possibleDeadNode);
		    					deadCount = 0;
		    				}
		    				
		    			} else {
		    				System.out.println("SHOULD BE REMOVING A NON DEAD NODE WITH ID: " + possibleDeadNode);
		    				if (maybeDead.contains(possibleDeadNode)) {
		    					maybeDead.remove(possibleDeadNode);
		    					System.out.println("REMOVED NODE FROM MAYBE DEAD LIST");
		    				}
		    				//System.out.println("NODE IS NOT DEAD: "  + possibleDeadNode);
		    			}
		    			//System.out.println("CURRENT INSTR IS: " + inStr);
		    			//System.out.println("PREVIOUS INSTR IS: " + seenContent.get(seenID));
		    			nodeCounter++;
		    		}
		    		
		    		
		    		
		    		
		    		
		    		
		    		/*
		    		 * FOR EVERY NEIGHBOUR CONTAINED IN PREVIOUS inStr:
		    		 * 
		    		 * 1. CHECK IF IT IS CONTAINED IN THE CURRENT inStr:
		    		 * 2. IF YES, DO CASE 1:
		    		 * 3. IF NO, SKIP TO CASE 2:
		    		 * 
		    		 * CASE 1:
		    		 * 
		    		 * 1. CHECK IF ARRAYLIST (POTENTIALLY DEAD) CONTAINS THE NODE.
		    		 * 2. IF SO, REMOVE IT (AS WE HAVE NOW SEEN IT AGAIN)
		    		 * 3. IF NOT, DO NOTHING
		    		 * 
		    		 * CASE 2:
		    		 * 1. SINCE ITS NOT CONTAINED IN THE PREVIOUS inStr, IT MUST BE POTENTIALLY DEAD
		    		 * 2. CHECK IF IT IS CONTAINED IN POTENTIALLY DEAD ARRAYLIST:
		    		 * 3. IF NOT, ADD IT TO THE ARRAYLIST
		    		 * 4. HERE WE HAVE SEEN A POTENTIALLY DEAD VALUE AND MUST RESET MESSAGE COUNT TO 0
		    		 * 
		    		 * 
		    		 * FINALLY:
		    		 * 1. IF MESSAGECOUNT == 3 * NEIGHBOUR_LIZE SIZE (AS BELOW)
		    		 * 2. THEN CHECK ENTIRE ARRAYLIST AND ANYTHING IN THERE MUSNT HAVE BEEN SEEN
		    		 * 3. IN ANY NEIGHBOURS MESSAGE FOR 3* NUMBER OF NEIGHBOURS MESSAGE COUNT
		    		 * 4. THUS, IT IS DEAD.
		    		 * 
		    		 * 
		    		 * REGARDLESS OF WHATEVER HAPPENS, RESET MESSAGE COUNT IN THIS AREA OF THE LOOP
		    		 * AS WE HAVE JUST SEEN A NEW MESSAGE FROM A NEIGHBOUR (AND THUS CONVERGENCE SHOULD BE FALSE
		    		 * AS WE HAVE JUST PUT A NEW COST OF 1 IN THE seenCosts HASHMAP. 
		    		 * 
		    		 * SINCE WE DONT WAN'T TO CHECK FOR DEAD NEIGHBOURS STRAIGHT AFTER A NEW MESSAGE FROM A NEIGHBOUR
		    		 * WE MUST RESET COUNT HERE
		    		 * 
		    		 */
		    		
		    		
		    		//messageCount = 0;
		    		seenCosts.put(seenID, 1);
			    	seenContent.put(seenID, inStr);
			    	seenCount.put(seenID, 1);
		    	}
		    }
		    
		    // EVERY TIME WE GET A MESSAGE
		    // CHECK NEIGHBOUR ID AND MESSAGE CONTENTS
		    // IF MESSAGE CONTENTS IS UNCHANGED FROM PREV
		    // ADD ONE TO COUNT
		     
		    
		    int wordCount = 0;
		    String neighbourID = "";
		    int nCost = 0;
		    // neighbour of neighbour
		    String nnID = "";
		    int nnCost = 0;
		    for (String s: words) {
		    	if (wordCount == 0) {
		    		neighbourID = s.replaceFirst(".* ", "");
		    		for (Neighbour n: neighbourList) {
		    			if (n.getID().equals(neighbourID)) {
		    				nCost = n.getCost();
		    				break;
		    			}
		    		}
		    		
		    	} else {
		    		nnCost = Integer.parseInt(s.replaceFirst(".* ", ""));
		    		nnID = s.replaceFirst(" .*", "");
		    		boolean containsDeletedNode = false;
		    		for (String nodeName: toDeleteNode) {
		    			if (nodeName.equals(nnID)) {
		    				containsDeletedNode = true;
		    				break;
		    			}
		    		}
		    		if (containsDeletedNode == false) {
		    			Neighbour newN = new Neighbour(nnID, nnCost + nCost, 0, neighbourID);
			    		nodeList.add(newN);
			    		//System.out.println("Path to node: " + nnID + " with cost " + nnCost + " via node: " + neighbourID);
		    		}
		    	}
		    	wordCount++;
		    }
		    
		    HashMap<String, Neighbour> seenHash = new HashMap<String, Neighbour>();
		    for (Neighbour n: nodeList) {
		    	//System.out.println("PATH TO NODE: " + n.getID() + " WITH COST: " + n.getCost() + " VIA NODE " + n.getV());
		    	if (!seenHash.containsKey(n.getID())) {
		    		seenHash.put(n.getID(), n);
		    	} else {
		    		if (seenHash.get(n.getID()).getCost() > n.getCost()) {
		    			seenHash.remove(n.getID());
		    			seenHash.put(n.getID(), n);
		    		}
		    	}
		    }
		    
		    
		    Collection <Neighbour> keptVals = seenHash.values();
		    nodeList.removeAll(nodeList);
		    nodeList.addAll(keptVals);
		    
		    //System.out.println("MESSAGE COUNT IS: " + messageCount);
		    messageCount++;
		    
		    if (messageCount == 3 * neighbourList.size()) {
		    	for (String key: seenCount.keySet()) {
		    		Integer count = seenCount.get(key);
		    		if (count > 0 ) {
		    			System.out.println("FOUND VALID COUNT");
		    			seenCount.put(key, 0);
		    			continue;
		    		} else {
		    			System.out.println("FOUND A DEAD NODE");
		    		}
		    		String foundNode = key;
		    		// found a node that hasn't sent a message in 3*number of neighbours messages received.
		    		// remove the neighbour from seen hash's that we check for convergence.
		    		
		    		//seenCosts.remove(foundNode);
		    		//seenContent.remove(foundNode);
		    		toDeleteNode.add(foundNode);
		    		
		    		// check every node in nodeList to see if its a path to the foundNode
		    		// or if its a path via the foundNode
		    		ArrayList <Neighbour> toDelete = new ArrayList<Neighbour>();
		    		// NOT DELETING THE PATH TO NODE CASE OR THE PATH VIA NODE CASE
		    		// CHECK TOMORROW AND FIX
		    		ArrayList <Neighbour> toRestore = new ArrayList<Neighbour>();
		    		for (Neighbour n: nodeList) {
		    			if (n.getID().equals(foundNode)) {
		    				toDelete.add(n);
		    			} else if (n.getV().equals(foundNode)) {
		    				String pathingTo = n.getID();
		    				for (Neighbour n1: neighbourList) {
		    					Boolean dontInclude = false;
		    					for (String s: toDeleteNode) {
		    						if (s.equals(n1.getID())) {
		    							dontInclude = true;
		    							break;
		    						}
		    					}
		    					if (dontInclude == true) continue;
		    					if (n1.getID().equals(pathingTo)) {
		    						Neighbour restored = new Neighbour(pathingTo, n1.getCost(), 0, n1.getV());
		    						toRestore.add(restored);
		    					}
		    				}
		    				toDelete.add(n);
		    			}
		    		}
		    		// remove all from nodeList in toDelete
		    		nodeList.removeAll(toDelete);
		    		nodeList.addAll(toRestore);
		    	}
		    	for (String s: toDeleteNode) {
		    		seenCosts.remove(s);
		    		seenContent.remove(s);
		    		seenCount.remove(s);
		    	}
		    	messageCount = 0;
		    }
		    
		    // WHENEVER WE FIND A DEAD NEIGHBOUR OR NEIGHBOUR OF NEIGHBOUR, REMOVE EVERYTHING AND 
		    // START AGAIN WITH THE DEAD NODES IN toDelete (so that paths to and via are always deleted).
		   
		    
		    if (deadCount >= neighbourList.size()) {
		    	System.out.println("CHECKING FOR DEAD NEIGHBOURS OF NEIGHBOURS");
		    	ArrayList<Neighbour> deadNodes = new ArrayList<Neighbour>();
		    	ArrayList <Neighbour> toRestore2 = new ArrayList<Neighbour>();
		    	for (String foundDead: maybeDead) {
		    		for (Neighbour n: nodeList) {
		    			if (n.getID().equals(foundDead)) {
		    				deadNodes.add(n);
		    			} else if (n.getV().equals(foundDead)) {
		    				// WHEN REMOVING VIA PATHS
		    				// CHECK OUR ORIGINAL NEIGHBOUR LIST
		    				// AND IF WE HAVE A DIRECT PATH TO THAT NEIGHBOUR
		    				// WE SHOULD ADD IT BACK INTO nodeList.
		    				
		    				// FOR EXAMPLE: NODE "C" SEE'S NODE "E" IS DEAD.
		    				// WE REMOVE ALL PATHS VIA E (WHICH INCLUDE A, D & F).
		    				// NOW C HAS NO INFORMATION REGARDING CHEAPEST COST TO THESE NODES
		    				// AND WILL ADD SOME STUPID VALUE TO ITS TABLE
		    				// INSTEAD WE MUST RE-INITIALISE THE STARTING VALUES FROM THE NEIGHBOUR LIST
		    				String pathingTo = n.getID();
		    				for (Neighbour n1: neighbourList) {
		    					if (n1.getID().equals(pathingTo)) {
		    						Neighbour restored = new Neighbour(pathingTo, n1.getCost(), 0, n1.getV());
		    						toRestore2.add(restored);
		    						
		    					}
		    				}
		    				deadNodes.add(n);
		    			}
		    		}
		    	}
		    	
		    	nodeList.removeAll(deadNodes);
		    	nodeList.addAll(toRestore2);
		    }
		    
		    // CONVERGENCE TEST IS BROKEN! FIX TOMORROW!
		    boolean converged = true;
		    for (Integer count: seenCosts.values()) {
		    	if (count < 9) {
		    		converged = false;
		    		convergeCount = 0;
		    		break;
		    	}
		    }
		    // line below just for debugging
		    //converged = true;
		    
		    if (converged) {
		    	if (convergeCount >= 0) {
			    	convergeCount++;
			    	
			    	System.out.println();
			    	System.out.println("SHORTEST PATHS TO EACH NODE ARE:");
			    	for (Neighbour n2: nodeList) {
			    		System.out.println("PATH TO NODE: " + n2.getID() + " WITH COST: " + n2.getCost() + " VIA NODE " + n2.getV());
			    	}
			    	/*
			    	System.out.println("RESET CONVERGENCE COUNT");
			    	
			    	messageCount = 0;
			    	for (String s: seenCosts.keySet()) {
			    		seenCosts.put(s, 0);
			    	}
			    	*/
		    	}
		    	// WE CAN JUST FIGURE OUT THE SHORTEST PATH LIST HERE
		    	// AND REMOVE EVERYTHING ELSE FROM NODELIST THAT WAY WE ONLY HAVE THE SHORTEST PATH AT ANY POINT IN
		    	// NODELIST, CAN ADD THE APPROPRIATE NODES AND PATH COSTS IN NODELIST EACH TIME, FIGURE OUT THE SHORTEST
		    	// PATH TO EACH NODE ID AND THEN REMOVE THE REST (AND REPEAT).
		    	
		    	// HASHMAP OF NODE IDS TO NODES. IF EXISTS(NODEID), CHECK COST AND IF CURRENT COST IS LESS
		    	// REPLACE NODE IN HASHMAP. AFTER ALL NODES SEEN, HASHMAP CONTAINS ALL NODES WITH THE SHORTEST PATHS
		    	// THUS, SET nodeList to be equal to all the values in the hashmap.
		    }
		}
	}
	
	private static void setTimer(Integer time, final ArrayList<Neighbour> neighbourList, final String nodeID) {
		// set timer here
		if (timer != null) {
			timer.cancel();
			timer.purge();
			System.out.println("Timer reset");
		}
		timer = new Timer();
		timer.scheduleAtFixedRate(new TimerTask() {
			public void run() {
				try {
					for (Neighbour n: neighbourList) {
						String ID = n.getID();
						String outString = "NEIGHBOUR " + nodeID + "/n";
						int cost = n.getCost();
						int port = n.getPort();
						//System.out.println("NEIGHBOURING NODE WITH ID: " + ID + ", COST: " + cost + " AND PORT: " + port);
						// CHANGE THIS TO SEND THE SHORTEST PATH LENGTH TO EVERY KNOWN NODE
						// VIA ITERATING THROUGH nodeList AND ONLY STORING THE SHORTEST PATH FOR EACH ID ENTRY
						
						// PROBABLY HAVE A NEW LIST CALLED SHORTEST PATH LIST AND JUST SEND THEM ALL OUT INSTEAD
						
						//for (Neighbour n2: neighbourList) {
						for (Neighbour n2: nodeList) {	
							String ID2 = n2.getID();
							if (ID2.equals(ID)) {
								continue;
							}
							int cost2 = n2.getCost();
							//int port2 = n2.getPort();
							//System.out.println("sending out data: '" + ID2 + " " + cost2 + "' " + "to node " + ID);
							outString += ID2 + " " + cost2 + "/n";
						}
						outString += '\n';
						
						byte[] out = new byte[bufferSize];
						String serverName = "localhost";
						InetAddress serverIPAddress = InetAddress.getByName(serverName);
						DatagramSocket neighbourSocket = new DatagramSocket();
						neighbourSocket.connect(serverIPAddress, port);
						out = outString.getBytes();
					    DatagramPacket outPacket = new DatagramPacket(out, out.length, serverIPAddress, port);
					    //System.out.println(outString);
					    neighbourSocket.send(outPacket);
					    neighbourSocket.close();
					}
					
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} finally {
					// done
				}
			}
		}, 1000 * time, 1000 * time);
	}
	
}
