
public class Neighbour {
	private String nodeID;
	private int pathCost;
	private int port;
	private String viaID;
	
	public Neighbour(String node, int cost, int port, String vID) {
		this.nodeID = node;
		this.pathCost = cost;
		this.port = port;
		this.viaID = vID;
	}
	
	public String getID () {
		return this.nodeID;
	}
	
	public int getCost () {
		return this.pathCost;
	}
	
	public int getPort () {
		return this.port;
	}
	
	public String getV(){
		return this.viaID;
	}
}
